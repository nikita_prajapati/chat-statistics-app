import { request } from "../utilities/request";

function getChatStatistics() {
    let url = window.location.origin + "/stubs/interview.json";
    // let url="http://45.79.111.106/interview.json";
    return request(url, "GET")
        .then(response => {
            return response;
        })
        .catch(error => {
            throw error;
        });
}

const apiService = {
    getChatStatistics
};

export default apiService;
