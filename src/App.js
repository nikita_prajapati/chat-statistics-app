import React, { useEffect, useState } from 'react';
import apiService from "./services/apiService";
import moment from 'moment';
import DatePicker from "react-date-picker";
import "./App.css";

function App() {

  const [interviewList, setInterviewList] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const getChatStatics = () => {
    apiService.getChatStatistics().then(response => {
      if (response && response.data) {
        setInterviewList(response.data);
      }
    }).catch(error => {
      console.log("error::", error)
    })
  }
  useEffect(getChatStatics, []);

  const filterInterviewData = () => {

    let date1 = new Date(startDate);
    let date2 = new Date(endDate);

    if (date1 && date2) {
      const newInterViewList = interviewList.filter(interview => {
        let date3 = new Date(interview.date);
        if (date3.getTime() >= date1.getTime() && date3.getTime() <= date2.getTime()) {
          return true;
        } else {
          return false;
        }
      });
      setInterviewList(newInterViewList);
    } else {
      getChatStatics();
    }
  }

  const clearFilter = () => {
    setStartDate(new Date());
    setEndDate(new Date());
    getChatStatics();
  }
  return (
    <div className="App">
      <h1>Chat Statics</h1>
      <div >
        <span className="date">Select Start Date:</span>
        <DatePicker value={startDate} onChange={date => setStartDate(date)} />
        <span className="date">Select End Date:</span>
        <DatePicker value={endDate} onChange={date => setEndDate(date)} />
        <span>
          <button className="btn btn-success" onClick={filterInterviewData}>GO</button>
        </span>
        <span>
          <button className="btn btn-primary" onClick={clearFilter}>Clear Filter</button>
        </span>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>websiteId</th>
            <th>date</th>
            <th>chats</th>
            <th>missedChats</th>
          </tr>
        </thead>
        <tbody>
          {interviewList.length > 0 ? (interviewList.map((interview, index) => {
            return (
              <tr key={index}>
                <td>{interview.websiteId}</td>
                <td>{moment(interview.date).format("DD-MM-YYYY")}</td>
                <td>{interview.chats}</td>
                <td>{interview.missedChats}</td>
              </tr>
            )
          })) : <div>No data found</div>}
        </tbody>
      </table>

    </div>
  );
}

export default App;
